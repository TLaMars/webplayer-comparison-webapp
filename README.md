# CMAF Video Player Web App

This is web app is build in [Next.js](https://nextjs.org/) for Thomas Lamars thesis about live video streaming in CMAF.

## Integrated Webplayers
In this web app there are multiple webplayers integrated for showing a CMAF live stream. The following webplayers are integrated:
- Bitmovin Player
- dash.js
- hls.js
- ShakaPlayer
- THEOPlayer

## Starting up this project
After cloning this project the packages need to be installed, run the following commmand to install the packages:
``` bash
npm install
```

Now that the packages are installed their are two options:
- Starting the development server
- Starting a production server

### Starting the development server
Starting the development server can be done by running:
```bash
npm run dev
```

An advantage of the development server is that when a change is made in the code. The development server will recompile the code and the changes will be visible in the browser directly.

### Starting a production server
To start a production server there is one more step needed before it can be started. First we need to create a production build. This can be done with:

```bash
npm run build
```

Then after the build is created, we can start the production server with:

```bash
npm run start
```
<br/><br/>



After that the development or production server is running you can access the server by going to: [http://localhost:3000](http://localhost:3000)
