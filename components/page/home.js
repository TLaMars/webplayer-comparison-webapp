import dynamic from "next/dynamic";
import { useState } from "react";
import VideoInput from "../ui/videoInput/videoInput";
import VideoChooser from "../ui/videoChooser/videoChooser";
import TargetLatency from "../ui/targetLatency/targetLatency";

// Players
import BitmovinPlayer from "../players/bitmovin";
import HlsPlayer from "../players/hls";
import TheoPlayerDash from "../players/theoDash";
import TheoPlayerHls from "../players/theoHls";
import TheoPlayerNormalHls from "../players/theoPlayerNormalHls";

const DashPlayer = dynamic(() => import('../players/dash'), { ssr: false });
const ShakaPlayer = dynamic(() => import('../players/shaka'), { ssr: false });

// Object with all players to dynamically show the choosen player
const players = (videoUrl, targetLatency) => {
    return [
        { name: "BitmovinPlayer", img: "/BitmovinPlayer.png", type: "mpd",  component: <BitmovinPlayer videoUrl={videoUrl} targetLatency={targetLatency} />  },
        { name: "DashPlayer",     img: "/dashjs.png",         type: "mpd",  component: <DashPlayer videoUrl={videoUrl} targetLatency={targetLatency} /> },
        { name: "HlsPlayer",      img: "/hlsjs.png",          type: "m3u8", component: <HlsPlayer videoUrl={videoUrl} targetLatency={targetLatency} /> },
        { name: "ShakaPlayer",    img: "/shaka-player.png",   type: "both", component: <ShakaPlayer videoUrl={videoUrl} /> },
        { name: "TheoPlayerDash", img: "/THEOplayer.png",     type: "mpd",  component: <TheoPlayerDash videoUrl={videoUrl} targetLatency={targetLatency} /> },
        { name: "TheoPlayerHls",  img: "/THEOplayer.png",     type: "m3u8", component: <TheoPlayerHls videoUrl={videoUrl} targetLatency={targetLatency} /> }
        // { name: "TheoPlayerNormalHls",  img: "/THEOplayerNormalLatency.png",     type: "m3u8", component: <TheoPlayerNormalHls videoUrl={videoUrl} /> }
    ]
};

export default function Home() {
    const [videoUrl, setVideoUrl] = useState(null);
    const [videoType, setVideoType] = useState(null);
    const [player, setPlayer] = useState(null);
    const [targetLatency, setTargetLatency] = useState(5);

    // Change the video url and set the correct video type
    const changeUrl = (input) => {
        setPlayer(null);
        setVideoUrl(input);

        if (input.includes("m3u8")) {
            setVideoType("m3u8");
        } else if (input.includes("mpd")) {
            setVideoType("mpd");
        } else {
            setVideoType(null);
            setVideoUrl(null);
        }
    }

    const changePlayer = (playerName) => {
        setPlayer(playerName);
    }

    const changeTargetLatency = (input) => {
        setTargetLatency(input)
    }

    return (
        <div className="container mx-auto mt-10">
            <VideoInput onChange={changeUrl} />

            { videoType && !player && <VideoChooser videoType={videoType} onClick={changePlayer} videoPlayers={players()} /> }

            { videoUrl && player && players(videoUrl, targetLatency).find(({ name }) => name == player).component }

            { player && <TargetLatency currentPlayer={player} onChange={changeTargetLatency} /> }
            
        </div>
    )
}
