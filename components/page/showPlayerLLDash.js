import dynamic from "next/dynamic";
import { useState } from "react";
import TargetLatency from "../ui/targetLatency/targetLatency";

const DashPlayer = dynamic(() => import('../players/dash'), { ssr: false });

export default function ShowPlayerLLDash() {
    const [targetLatency, setTargetLatency] = useState(1);
    const changeTargetLatency = (input) => {
        setTargetLatency(input)
    }

    return (
        <div className="container mx-auto mt-10">
            <DashPlayer videoUrl="https://lowlatency-test.akamaized.net/cmaf/live-ull/2031932/event/dash.mpd" targetLatency={targetLatency} />
        </div>
    )
}
