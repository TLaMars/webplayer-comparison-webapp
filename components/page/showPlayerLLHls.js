import { useState } from "react";
import HlsPlayer from "../players/hls";

export default function ShowPlayerLLHls() {
    const [targetLatency, setTargetLatency] = useState(1);
    const changeTargetLatency = (input) => {
        setTargetLatency(input)
    }

    return (
        <div className="container mx-auto mt-10">
            <HlsPlayer videoUrl="https://lowlatency-test.akamaized.net/cmaf/live-ull/2031932/event/index.m3u8" targetLatency={targetLatency} />
        </div>
    )
}
