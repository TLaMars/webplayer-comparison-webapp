import { Player } from 'bitmovin-player';
import { useCallback, useEffect, useRef, useState } from 'react';
import LatencyShower from '../ui/latencyShower/latencyShower';

// Bitmovin player config
const config = {
    key: "1768d129-7dd4-4fd3-8970-7c382d0cca5b",
    location: {
        ui: 'https://cdn.bitmovin.com/player/web/8/bitmovinplayer-ui.js',
        ui_css: 'https://cdn.bitmovin.com/player/web/8/bitmovinplayer-ui.css',
    },
    playback: {
        autoplay: true,
        muted: true,
    },
    adaptation: {
        preload: false,
    },
    style: {},
    tweaks: {
        RESTART_THRESHOLD: 0.2,
        RESTART_THRESHOLD_DELTA: 0.05,
        STARTUP_THRESHOLD: 0.8,
        STARTUP_THRESHOLD_DELTA: 0.05,
        CHUNKED_CMAF_STREAMING: true
    },
    live: {
        lowLatency: {
            targetLatency: 5,
            catchup: {
                playbackRateThreshold: 0.065,
                seekThreshold: 2,
                playbackRate: 1.4,
            },
            fallback: {
                playbackRateThreshold: 0.065,
                seekThreshold: 2,
                playbackRate: 0.5,
            }
        }
    }
}

export default function BitmovinPlayer({ videoUrl, targetLatency }) {
    const bitmovinPlayer = useRef();

    const [player, setPlayer] = useState(undefined);

    // Hook to create the player
    useEffect(() => {
        const player = new Player(bitmovinPlayer.current, config);
        player.load({ dash: videoUrl }).then(() => {
            player.play();
        }).catch(() => {})

        setPlayer(player);

        return function cleanup() {
            player.unload();
        }
    }, [])

    // Hook to update the target latency
    useEffect(() => {
        if (player) {
            player.lowlatency.setTargetLatency(targetLatency);
        }
    }, [targetLatency, player]);

    const getLatencyCB = useCallback(() => {
        return {
            latency: Math.round(player.lowlatency.getLatency() * 1000) / 1000,
            buffer: (player.getVideoBufferLength() || 0).toFixed(3)
        }
    }, [player]);

    return (
        <>
            <h1>Bitmovin Player</h1>
            <div ref={bitmovinPlayer}></div>
            { player && <LatencyShower getLatency={getLatencyCB} /> }
        </>
    )
}
