import dashjs from 'dashjs';
import { useCallback, useEffect, useRef, useState } from 'react';
import LatencyShower from '../ui/latencyShower/latencyShower';

// dashJS player config
var config = {
    streaming: {
        lowLatencyEnabled: true, 
        liveDelay: 1.5,
        liveCatchup: {
            minDrift: 0.02,
            maxDrift: 0,
            playbackRate: 0.5,
            latencyThreshold: 30,
            playbackBufferMin: 0.5    
        }
    }
}

export default function DashPlayer({ videoUrl, targetLatency }) {
    const dashPlayer = useRef();

    const [player, setPlayer] = useState(undefined);
    
    // Hook to create the player
    useEffect(() => {
        const player = dashjs.MediaPlayer().create();

        player.initialize(dashPlayer.current, videoUrl, true);
        player.updateSettings(config);

        setPlayer(player);

        return function cleanup() {
            player.destroy();
        }
    }, []);

    // Hook to update the target latency
    useEffect(() => {
        if (player) {
            player.updateSettings({
                streaming: {
                    liveDelay: targetLatency
                }
            })
        }
    }, [targetLatency, player]);

    const getLatencyCB = useCallback(() => {
        return {
            latency: player.getCurrentLiveLatency(),
            buffer: player.getBufferLength()
        }
    }, [player]);

    return (
        <>
            <h1>dashjs Player</h1>
            <div>
                <video ref={dashPlayer} controls></video>
                { player && <LatencyShower getLatency={getLatencyCB} /> }
            </div>
        </>
    )
}