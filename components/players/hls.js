import Hls from 'hls.js';
import { useCallback, useEffect, useRef, useState } from 'react';
import LatencyShower from '../ui/latencyShower/latencyShower';

const config = (targetLatency) => {
    return {
        "liveSyncDuration": targetLatency,
        "maxLiveSyncPlaybackRate": 1.5,
        "autoPlay": true,
        "lowLatencyMode": true
    }
}

export default function HlsPlayer({ videoUrl, targetLatency }) {
    const hlsPlayer = useRef();

    const [player, setPlayer] = useState(undefined);

    // Hook to create the player
    useEffect(() => {
        const player = new Hls(config(targetLatency));
        player.loadSource(videoUrl);
        player.attachMedia(hlsPlayer.current);
        
        setPlayer(player);

        return function cleanup() {
            player.destroy();
        }
    }, []);

    useEffect(() => {
        if (player) {
            player.config.liveSyncDuration = targetLatency;
        }
    }, [targetLatency, player]);

    const getLatencyCB = useCallback(() => {
        return {
            latency: player.latency.toFixed(3),
            buffer: 0
        }
    }, [player]);

    return (
        <>
            <h1>Hls Player</h1>
            <div>
                <video ref={hlsPlayer} controls autoPlay></video>
                { player && <LatencyShower getLatency={getLatencyCB} /> }
            </div>
        </>
    )
}
