import { createRef, useEffect } from 'react';
const shaka = require('shaka-player/dist/shaka-player.ui.js');

export default function ShakaPlayer({ videoUrl }) {
    const shakaVideo = createRef();
    const shakaContainer = createRef();

    // Hook to create the player
    useEffect(() => {
        let video = shakaVideo.current;
        let videoContainer = shakaContainer.current;

        const player = new shaka.Player(video);
        const ui = new shaka.ui.Overlay(player, videoContainer, video);

        player.configure({
            streaming: {
                lowLatencyMode: true
            }
        })

        player.load(videoUrl)

        return function cleanup() {
            player.destroy();
        }
    }, []);

    return (
        <>
            <h1>ShakaPlayer</h1>
            <div ref={shakaContainer}>
                <video id="video" ref={shakaVideo} className="w-full h-full" autoPlay muted></video>
            </div>
        </>
    );
}
