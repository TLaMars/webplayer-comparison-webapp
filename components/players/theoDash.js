import React, { useCallback, useEffect, useRef, useState } from "react";
import LatencyShower from "../ui/latencyShower/latencyShower";

// THEOPlayer Dash config
var appConfig = (targetLatency = 2) => { 
    return {
        "config" : [{
            "targetlatency" : targetLatency * 1000,
            "seekwindow"    : 2500,
            "latencywindow" : 100,
            "encoderlatency" : 0,
            "useencodertime" : false,
            "interval"      : 40,
            "fireupdate"    : false,
            "ratechange"    : 0.5,
            "sync"          : true,
            "disableonpause" : false,
            "timeserver": "https://time.akamai.com/?iso&ms"
        }]
    }
};

export default function TheoPlayerDash({ videoUrl, targetLatency }) {
    const theoPlayer = useRef();

    const [latencyManager, setLatencyManager] = useState(undefined);
    const [liveLatency, setLiveLatency] = useState(0);
    
    // Hook to create the player and latency manager
    useEffect(() => {
        const player = new THEOplayer.Player(theoPlayer.current, {
            libraryLocation: "https://cdn.myth.theoplayer.com/ca18825f-b662-4b9d-8920-c13dec16575b/",
            license: process.env.THEOPLAYER_KEY
        });

        player.source = {
            sources: [{
                src: videoUrl,
                type: 'application/dash+xml',
                lowLatency: true,
                liveOffset: 1.0
            }]
        };

        const latencymanager = THEOplayer.initializeLatencyManager(player, appConfig);
        latencymanager.configure(appConfig(targetLatency).config[0]);
        setLatencyManager(latencymanager);

        return function cleanup() {
            latencymanager.stop();
            player.destroy();
        }
    }, []);

    // Hook to update the targetLatency
    useEffect(() => {
        if (latencyManager) {
            latencyManager.configure(appConfig(targetLatency).config[0]);
        }
    }, [targetLatency, latencyManager]);

    const getLatencyCB = useCallback(() => {
        return {
            latency: latencyManager.currentlatency,
        }
    }, [latencyManager]);

    return (
        <>
            <h1>THEOPlayer LL-DASH</h1>
            <div id="player" ref={theoPlayer} className="theoplayer-container video-js theoplayer-skin vjs-16-9 THEOplayer"></div>
            { latencyManager && <LatencyShower getLatency={getLatencyCB} /> }
        </>
    )
}
