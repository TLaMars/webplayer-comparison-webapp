import React, { useEffect, useRef, useState } from "react";

// THEOPlayer HLS config
var config = (targetLatency = 1) => {
    return {
        latencyTarget: targetLatency * 1000,
        latencyWindow: 250,
        latencyCatchupRate: 0.5,
        latencySeekWindow: 2500
    }
}

export default function TheoPlayerNormalHls({ videoUrl }) {
    const theoPlayer = useRef();
    const [player, setPlayer] = useState(undefined);

    // Hook to create the player and setup low latency
    useEffect(() => {
        const player = new THEOplayer.Player(theoPlayer.current, {
            libraryLocation: "https://cdn.myth.theoplayer.com/ca18825f-b662-4b9d-8920-c13dec16575b/",
            license: process.env.THEOPLAYER_KEY
        });

        player.source = {
            sources: [{
                src: videoUrl,
                type: 'application/x-mpegurl',
                lowLatency: true
            }]
        };

        setPlayer(player);

        return function cleanup() {
            player.destroy();
        }
    }, [])

    return (
        <>
            <div>
                <h1>THEOPlayer No Low Latency HLS</h1>
                <div id="player" ref={theoPlayer} className="theoplayer-container video-js theoplayer-skin vjs-16-9 THEOplayer"></div>
            </div>
        </>
    )
}
