import styles from "./input.module.css";

export default function Input({ onChange, onKeyPress, placeholder, name, value, size, type }) {
    return (
        <input 
            type={type}
            name={name}
            placeholder={placeholder}
            className={styles.input + " " + styles[size]}
            value={value}
            onChange={onChange}
            onKeyPress={onKeyPress}
        />

    )
}