import { useEffect, useState } from "react";

export default function LatencyShower({ getLatency }) {
    const [liveLatency, setLiveLatency] = useState(0);
    const [bufferLength, setBufferLength] = useState(null);
    
    useEffect(() => {
        const interval = setInterval(() => {
            const data = getLatency();
            setLiveLatency(data.latency);
            if ('buffer' in data) {
                setBufferLength(data.buffer);
            }
        }, 200);

        return function cleanup() {
            clearInterval(interval);
        }
    }, [])

    return (
        <div>
            <h3>Live video stats</h3>
            <p>Live latency: <strong id="stats_latency">{ liveLatency } s</strong></p>
            { bufferLength !== null && <p>Buffer length: <strong>{ bufferLength } s</strong></p> }
        </div>
    )
}