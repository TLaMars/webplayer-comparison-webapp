import { useState } from "react";
import Input from "../input/input";
import styles from "./targetLatency.module.scss";

export default function TargetLatency({ currentPlayer, onChange }) {
    const [input, setInput] = useState();

    const changeInput = (e) => {
        setInput(e.target.value);
    }

    const changeTargetLatency = () => {
        onChange(parseFloat(input));
    }

    if (currentPlayer === "ShakaPlayer" || currentPlayer === "TheoPlayerNormalHls") {
        return (
            <p>This player doesn't support changing the target latency.</p> 
        )
    }

    return (
        <div className={styles.targetLatency}>
            <Input 
                type="number"
                size="small"
                name="TargetLatency"
                placeholder="Choose a target latency in seconds"
                onChange={changeInput}
            />
            <button onClick={changeTargetLatency}>Change Target Latency</button>
        </div>
    )
}