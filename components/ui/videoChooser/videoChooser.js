import styles from "./videoChooser.module.scss";

export default function VideoChooser({ videoType, onClick, videoPlayers }) {
    const possiblePlayers = videoPlayers.filter(player => videoType === player.type || player.type === "both" && videoType);

    const buttonClick = (e) => {
        onClick(e.currentTarget.value);
    }

    return (
        <div className={styles.videoChooser}>
            <h1>Choose your webplayer</h1>
            <div className={styles.videoChooser__container}>
                { possiblePlayers.map((player) => {
                    return (
                        <button key={player.name} onClick={buttonClick} value={player.name}>
                            <img 
                                src={player.img}
                                alt={player.name}
                                className={styles.videoChooser__image}
                            />
                        </button>
                    )
                })}
            </div>
        </div>
    )
}