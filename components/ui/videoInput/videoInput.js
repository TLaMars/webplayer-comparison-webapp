import { useState } from "react";
import Input from "../input/input";
import styles from "./videoInput.module.scss";

export default function VideoInput({ onChange }) {
    const [input, setInput] = useState();

    const changeInput = (e) => {
        setInput(e.target.value);
    }

    const setUrl = () => {
        onChange(input);
    }
    
    return (
        <div className={styles.videoInput}>
            <Input 
                type="text"
                name="videoInput"
                placeholder="Place your HLS or MPEG-DASH url here"
                onChange={changeInput}
            />
            <button onClick={setUrl}>Compatible Players</button>
        </div>

    )
}