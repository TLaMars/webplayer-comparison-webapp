module.exports = {
    env: {
        BITMOVIN_KEY: process.env.BITMOVIN_KEY,
        ACKEE_KEY: process.env.ACKEE_KEY,
        BACKEND_BASE_URL: process.env.BACKEND_BASE_URL,
        SENTRY_DSN: process.env.SENTRY_DSN,
        THEOPLAYER_KEY: process.env.THEOPLAYER_KEY
    },
    target: "serverless"
};
