import '../styles/globals.css';
import 'shaka-player/dist/controls.css';
import Head from 'next/head';


function MyApp({ Component, pageProps }) {
    return (
        <>
            <Head>
                <title>Video Player Web App</title>
                <link rel="icon" href="/favicon.ico" />
                <link rel="stylesheet" href="https://cdn.myth.theoplayer.com/ca18825f-b662-4b9d-8920-c13dec16575b/ui.css"/>
                <script type='text/javascript' src='https://cdn.myth.theoplayer.com/ca18825f-b662-4b9d-8920-c13dec16575b/THEOplayer.js'></script>
                <script src="https://www.theoplayer.com/hs/hsstatic/jquery-libs/static-1.4/jquery/jquery-1.11.2.js"></script>
                <script type='text/javascript' src='latencymanager.js'></script>
                <script src="https://www.theoplayer.com/hs/hsstatic/jquery-libs/static-1.4/jquery/jquery-1.11.2.js"></script>
                <script type="text/javascript" src="ll-hls-latency-manager-module.js"></script>
            </Head>
            <Component {...pageProps} />
        </>
    )
}

export default MyApp
