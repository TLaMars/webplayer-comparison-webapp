(function(window) {
    var latencyConfig = {};
    var player = null;
    var isSetup = false;

    THEOplayer.setupLLHLS = function(config, videoPlayer) {
        clearInterval(latencyManagerIntervalId);
        clearInterval(playerInfoUpdateLoopId);

        latencyConfig = config;
        player = videoPlayer
        setup();
    }

    THEOplayer.updateLatency = function(targetLatency) {
        clearInterval(latencyManagerIntervalId);
        clearInterval(playerInfoUpdateLoopId);
        isSetup = false;

        latencyConfig.latencyTarget = targetLatency * 1000;

        setup();
    }

    THEOplayer.stopLLHLS = function() {
        clearInterval(latencyManagerIntervalId);
        clearInterval(playerInfoUpdateLoopId);
        isSetup = false;
    }

    var akamaiTime;
    var timeWhenAkamaiRequest;
    var latencyManagerIntervalId;
    var playerInfoUpdateLoopId;

    function setupErrorListener(player) {
        player.addEventListener('error', function (e) {
            if (e.error === "The video stream has been reset. THEOplayer will attempt to reload the source.") {
                player.source = player.source;
                player.play();
            }
        });
    }

    function calculateCurrentProgramDt() {
        // New function to grab playback time
        return new Date(player.currentProgramDateTime).getTime();
    }

    function calculateLatency() {
        // return difference current time and playback time
        var now = calculateCurrentTimestamp();
        var playbackTime = calculateCurrentProgramDt();
        return (now.getTime() - playbackTime);
    }

    function calculateBufferSize() {
        var MARGIN = 0.04;
        var buffered = player.buffered;
        var currentTime = player.currentTime;

        var searchingStartIndex = true;
        var bufferSize = 0;
        var bufferedCalculationEnd = 0;
        for (var i = 0; i < buffered.length; i += 1) {
            var start = buffered.start(i) - MARGIN;
            var end = buffered.end(i) + MARGIN;

            if (searchingStartIndex) {
                if (start < currentTime) {
                    searchingStartIndex = false;
                    bufferSize = Math.max(0, end - currentTime);
                }
            } else {
                if (bufferedCalculationEnd > start) {
                    bufferSize = Math.max(0, end - currentTime);
                } else {
                    return bufferSize;
                }
            }
            bufferedCalculationEnd = end;
        }

        return bufferSize;
    }

    function stopLatencyTuning() {
        clearInterval(latencyManagerIntervalId);
        player.playbackRate = 1;
    }

    function setupLatencyTuning() {
        var LATENCY_LOOP_INTERVAL = 500;
        var latencyTarget = parseInt(latencyConfig.latencyTarget) || 4000;
        var latencyWindow = parseInt(latencyConfig.latencyWindow) || 250;
        var latencySeekWindow = parseInt(latencyConfig.latencySeekWindow) || 5000;
        var latencyCatchupRate = parseFloat(latencyConfig.latencyCatchupRate) || 0.08;

        latencyManagerIntervalId = setInterval(latencyUpdateLoop, LATENCY_LOOP_INTERVAL);

        function latencyUpdateLoop() {
            if (player.paused) {
                return;
            }

            var currentLatency = calculateLatency();
            var shouldWeSeek = latencyTarget - latencySeekWindow > currentLatency || latencyTarget + latencySeekWindow < currentLatency;
            var shouldWeSpeedUp = latencyTarget - latencyWindow > currentLatency || latencyTarget + latencyWindow < currentLatency;
            var shouldSpeedUp = currentLatency > latencyTarget;
            var playbackRateIsAlreadyChanged = shouldSpeedUp ? player.playbackRate === (1 + latencyCatchupRate) : player.playbackRate === (1 - latencyCatchupRate);

            if (shouldWeSeek) {
                player.currentTime += (currentLatency - latencyTarget) / 1000;
            } else if (shouldWeSpeedUp && !playbackRateIsAlreadyChanged) {
                player.playbackRate = shouldSpeedUp ? 1 + latencyCatchupRate : 1 - latencyCatchupRate;
            } else if (!shouldWeSpeedUp && playbackRateIsAlreadyChanged) {
                player.playbackRate = 1
            }
        }
    }

    function formatInMiliseconds(value) {
        return Math.floor(value) / 1000 + ' s';
    }

    function formatDateTime(date) {
        return date.toISOString();
    }

    function setupPlayerInfo() {
        var INFO_LOOP_INTERVAL = 250;
        var statsLatency = document.getElementById('stats_latency');

        function playerInfoUpdateLoop() {
            if (player.paused) {
                if ( statsLatency ) {
                    statsLatency.innerText = '-';
                }
            } else {
                updateLatencyValue();
            }
        }

        function updateLatencyValue() {
            const currentLatency = calculateLatency();
            const currentBuffer = calculateBufferSize() * 1000;

            if ( statsLatency ) {
                statsLatency.innerText = formatInMiliseconds(calculateLatency());
            }
        }

        playerInfoUpdateLoopId = setInterval(playerInfoUpdateLoop, INFO_LOOP_INTERVAL);
    }

    function setup() {
        if (isSetup == false) {
            setSource();
            setupErrorListener(player);

            player.addEventListener('play', function () { 
                player.currentTime = Infinity 
            });

            setupPlayerInfo();
            setupLatencyTuning();
            requestAkamaiTime();

            isSetup = true;
        }
    }

    function resetLLHLS() {
        
    }

    function requestAkamaiTime() {
        $.ajax({
            url: "https://time.akamai.com/?iso&ms",
            type: 'get',
            async: false,
            success: function (data) {
                const date = new Date(data);
                akamaiTime = date.getTime();
                timeWhenAkamaiRequest = performance.now();
            }
        });
    }

    function calculateCurrentTimestamp() {
        const diff = performance.now() - timeWhenAkamaiRequest;
        return new Date(akamaiTime + diff);
    }

    function setSource() {
        stopLatencyTuning();
    }

})(window);